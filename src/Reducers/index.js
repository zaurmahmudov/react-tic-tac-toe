import { combineReducers } from "redux";
import MovesReducer from "./MovesReducer";
import OrderReducer from "./OrderReducer";
import WinnerReducer from "./WinnerReducer";

const Index = combineReducers({
    'moves': MovesReducer,
    'order': OrderReducer,
    'winner': WinnerReducer,
})

export default Index