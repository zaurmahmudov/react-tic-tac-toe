// OrderReducer tracks whos turn is to play, 0 for man, 1 for computer
const OrderReducer = (state = 0, action) => {
    switch(action.type) {
        case "SWITCH_PLAYER":
            return state === 1?0:1;
        case "RESTART_PLAY":
            return 0;
        default:
            return state;
    }
}

export default OrderReducer