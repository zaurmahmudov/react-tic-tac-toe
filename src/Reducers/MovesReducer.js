const MovesReducer = (state = [], action) => {
    switch(action.type) {
        case "ADD_MOVE":
            return [...state, action.payload];
        case "CLEAN_MOVES": 
            return action.payload;
        default:
            return state;
    }
}

export default MovesReducer;