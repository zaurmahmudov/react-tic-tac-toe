const WinnerReducer = (state = [], action) => {
    switch(action.type) {
        case "SET_WINNER":
            return [...action.payload];
        default: 
            return []
    }
}

export default WinnerReducer