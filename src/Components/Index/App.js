import './App.scss'
import Board from '../Board/Board'
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import Store from '../../Reducers'



function App() {

  const store = createStore(
      Store,
      window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
  return (
    <Provider store={store}>
      <div className="main">
        <Board />
      </div>
    </Provider>
  );
}

export default App;
