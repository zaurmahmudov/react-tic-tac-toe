import React, { Component } from 'react';
import Cell from '../Cell/Cell';
import { connect } from 'react-redux';
import './Board.scss';
import { set_winner } from '../../Action/MovesActions';
const ComputerMove = require('../../Logic/ComputerMove');


class Board extends Component {
    constructor (props) {
        super(props);
    }

    initCells = () => {
        const cells = [];
        for(var x=1;x<10; x++){
            
            cells.push(<Cell cellOrder={x} key={x} />)
        }
        return cells;
    }
    componentWillReceiveProps(nextProps) {
        setTimeout(() => {
            // Initiate Computer Move
            
                const computer = new ComputerMove(this.props.moves);
                const outcome = computer.getOutCome();
                if(outcome.cells) {
                    this.props.set_winner(outcome.cells);
                }
            
        },50)
    }
    render() {
        return (
            <div className="board">
                {this.initCells()}
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        'moves': state.moves,
        'order': state.order,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        set_winner: (arr) => dispatch(set_winner(arr)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Board);