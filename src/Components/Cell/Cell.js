import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Cell.scss';
import {add_move} from '../../Action/MovesActions';
import {switch_player} from '../../Action/OrderActions';
const ComputerMove = require('../../Logic/ComputerMove');


class Cell extends Component {
    constructor(props){
        super(props);
        this.state = {
            cellOrder: props.cellOrder,
            context: '',
            winningCell: false,
        }
    }

    componentWillReceiveProps(nextProps) {
        nextProps.moves.map((ele) => {
            if(this.state.cellOrder === ele.cellOrder) {
                ele.player === 0 ? this.setState({context:'x'}) : this.setState({context:'o'})
            }
        })
        if(nextProps.winner) {
            nextProps.winner.map(w => {
                if(this.state.cellOrder === w) {
                    this.setState({winningCell:true});
                }
            })
        }
    }
    makeMove = () => {
        if(!!this.state.context) return false;
        
        this.props.order === 0 ? this.setState({context:'x'}) : this.setState({context:'o'})
        this.props.add_move(this.state.cellOrder,this.props.order);
        this.props.switch_player();
        
        setTimeout(() => {
            // Initiate Computer Move
            if(this.props.order === 1) {
                const computer = new ComputerMove(this.props.moves);
                const move = computer.move();
                this.props.add_move(move,this.props.order);
                this.props.switch_player();
            }
            
        },100)
    }

    render() {
        return (
            <div className={this.state.winningCell?'cell cell--winner':'cell'} onClick={this.makeMove}>
                {this.state.context}
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        'moves': state.moves,
        'order': state.order,
        'winner': state.winner,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        add_move: (cellOrder, player) => dispatch(add_move(cellOrder,player)),
        switch_player: () => dispatch(switch_player()),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Cell);