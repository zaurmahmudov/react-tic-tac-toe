class ComputerMove {
    constructor(positions) {
        this.positions = positions;
        this.humanMoves = [];
        this.computerMoves = [];
        // Win scenarios
        this.winScenarios = [
            [1,2,3],
            [4,5,6],
            [7,8,9],
            [1,4,7],
            [2,5,8],
            [3,6,9],
            [1,5,9],
            [3,5,7],
        ];

    }

    dividePositions = () => {
        this.positions.forEach(p => {
            if(p.player === 0) this.humanMoves.push(p.cellOrder);
            if(p.player === 1) this.computerMoves.push(p.cellOrder);
        })
    }
    possibleWins = () => {
        if(this.computerMoves.length < 2) return null;
        let move = null;
        this.winScenarios.forEach(scenario => {
            const intersections = scenario.filter(element => this.computerMoves.includes(element))
            if(intersections.length === 2) {
                const winMove =  scenario.filter(element => !intersections.includes(element))
                if(!this.humanMoves.includes(winMove[0])){
                    move = winMove[0]
                }
            }
        })
        return move;
    }
    possibleLosses = () => {
        if(this.humanMoves.length < 2) return null;
        let move = null;
        this.winScenarios.forEach(scenario => {
            const intersections = scenario.filter(element => this.humanMoves.includes(element))
            if(intersections.length === 2) {
                const winMove =  scenario.filter(element => !intersections.includes(element))
                if(!this.computerMoves.includes(winMove[0])){
                    move = winMove[0]
                }
            }
        })
        return move;
    }

    randomCorder = () => {
        let corners = [1,3,7,9];
        corners = corners.filter(c => !this.humanMoves.includes(c)) 
        corners = corners.filter(c => !this.computerMoves.includes(c)) 
        if(corners.length === 0){
            return this.randomEmpty();
        }
        return corners[Math.floor(Math.random() * corners.length)];
    }

    randomEmpty = () => {
        let cells = [1,2,3,4,5,6,7,8];
        cells = cells.filter(c => !this.humanMoves.includes(c)) 
        cells = cells.filter(c => !this.computerMoves.includes(c)) 
        return cells[Math.floor(Math.random() * cells.length)];
    }
    


    move() {
        // Itirate position and assigned them to humanMoves / computerMoves
        this.dividePositions();
        // We should identify if there any way we can win with this move
        const winMove = this.possibleWins();
        if(winMove) return winMove;
        // Whether or not Human can have already 2 in one line Prevent Human of playing there.
        const loseMove = this.possibleLosses();
        if(loseMove) return loseMove;
        // Start in one of 4 corners
        return this.randomCorder();
    }


    getOutCome = () => {
        
        // Itirate position and assigned them to humanMoves / computerMoves
        this.dividePositions();
        // Check Human
        let cells = [];
        let human = false;
        this.winScenarios.forEach(scenario => {
            const intersections = scenario.filter(element => this.humanMoves.includes(element))
            if(intersections.length === 3) {
                // Human won
                cells = intersections
                human = true;
            }
        })

        if(human) return {outcome: 'human',cells:cells};
        // Check Computer
        let computer = false;
        this.winScenarios.forEach(scenario => {
            const intersections = scenario.filter(element => this.computerMoves.includes(element))
            if(intersections.length === 3) {
                // Computer won
                cells = intersections
                computer = true;
            }
        })
        if(computer) return {outcome: 'computer',cells:cells};
        // If Draw
        if(this.positions.length === 9) {
            return {outcome: 'draw',cells: []};
        }
        return {outcome: null,cells: []};
    }
}

module.exports = ComputerMove;