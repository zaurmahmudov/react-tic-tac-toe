export const add_move = (cellOrder, player) => {
    return {
        type: "ADD_MOVE",
        payload: {
            cellOrder,
            player
        }
    }
}

export const clean_moves = () => {
    return {
        type: "CLEAN_MOVES",
        payload: []
    }
}

export const set_winner = (arr) => {
    return {
        type: "SET_WINNER",
        payload: arr
    }
}