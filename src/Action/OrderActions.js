export const switch_player = () => {
    return {
        type: "SWITCH_PLAYER",
        payload: []
    }
}

export const restart_play = () => {
    return {
        type: "RESTART_PLAY",
        payload: []
    }
}
